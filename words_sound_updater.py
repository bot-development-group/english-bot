import os
from google.cloud import texttospeech
import sql
import pickle
import concurrent.futures as concurrent

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = ''  # path to your json token file

VOICE = texttospeech.types.VoiceSelectionParams(
    language_code='en-US',
    name='en-US-Wavenet-D')

# Select the type of audio file you want returned
AUDIO_CONFIG = texttospeech.types.AudioConfig(
    audio_encoding=texttospeech.enums.AudioEncoding.MP3)


def get_sound(client, text):
    synthesis_input = texttospeech.types.SynthesisInput(text=text)

    response = client.synthesize_speech(synthesis_input, VOICE, AUDIO_CONFIG)
    return response.audio_content


def db_sound_fetch(word_tuple):
    client = texttospeech.TextToSpeechClient()

    sound = get_sound(client, word_tuple[1])

    with sql.get_connection() as connection:
        with connection.cursor() as cursor:
            try:
                cursor.execute("INSERT INTO words_sound (id, sound) VALUES (%s, %s)", (word_tuple[0], sound))
                connection.commit()
            except Exception as e:
                print(e)


if __name__ == '__main__':
    with sql.get_connection() as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT w.id, w.word_en FROM "
                       "words AS w "
                       "LEFT JOIN words_sound AS s ON w.id=s.id "
                       "WHERE s.id IS null;")
        words = cursor.fetchall()
    with open('words.pickle', 'wb') as f:
        pickle.dump(words, f)
    with open('words.pickle', 'rb') as f:
        words = pickle.load(f)

    print(words)

    with concurrent.ProcessPoolExecutor(max_workers=10) as executor:
        executor.map(db_sound_fetch, words, timeout=10)
