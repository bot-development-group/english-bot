from states.errors import *
from sql import get_test_names
from utils import create_markup
from constants import coach_fields

"""
Simple use of test handlers
    the last one is necessary for test init
    the first one is handle all test by user's state
        (for test it is caching in states obj, db doesn't use at all)
"""


def test_handlers(bot, states, test_name='test_'):
    """
    Wrapper for a case of using
    :param bot: <telebot.Telebot>
    :param states: <state.States>
    :param test_name <str>
    """

    @bot.message_handler(func=lambda message: states.handle(message, "choosing_test"))
    def test_start(message):
        for test in get_test_names():
            if test[1] in message.text:
                # last arg is test id (may use simple number (1 in that case))
                states.start_test(message, bot, test[0])

    @bot.callback_query_handler(func=lambda x: states.handle(x, test_name))
    def test_test(message):
        try:
            res = states.handle_test(message, bot, states.db.get_state(message.from_user.id).split("_")[1:])
            # Condition for answers handle
            if res is not None:
                states.change_state(message, 'menu')
                bot.send_message(message.from_user.id, f'Тест завершен!\nРезультат: {res}',
                                 parse_mode="Markdown", reply_markup=create_markup(coach_fields[-2:], 2))
        except UserNotFoundException:
            states.change_state(message, 'menu')
