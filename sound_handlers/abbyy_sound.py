import requests
import wave
import base64
import os

def get_bear_token(api_token):
    """
    Return bearer token for abbyy api (for all calls instead of auth)
    :param api_token: <str> abbyy token
    :return: <str> bearer token for api calls
    """
    auth_resp = requests.post("https://developers.lingvolive.com/api/v1.1/authenticate",
                              headers={'Authorization': f"Basic {api_token}"})
    if auth_resp.status_code == 200:
        return auth_resp.text
    else:
        raise Exception("Unable to get bear token from abbyy service")


def abbyy_get_wav(word, lang, api_token=None, bearer_token=None, custom_file_name=None):
    """
    Saves sound for word ans saves it in a .wav file into (./abbyy_sounds directory)
    :param word: <str> word, whick sound you want to get
    :param lang: <str> word's lang (rus,eng)
    :param api_token: <str> auth token for abbyy (bearer_token will be get by call)
    :param bearer_token: <str> token for abbyy api calls
    :param custom_file_name: <str> to change file name (rus words saving with numbers names,so it is a quite good)
    :return: <str> filename of sound file
    """
    if bearer_token is None and api_token is None:
        raise Exception("No api token was found")

    if bearer_token is None:
        bearer_token = get_bear_token(api_token)

    languages = {
        'eng': [1033, 1049],
        'rus': [1049, 1033]
    }

    translate_resp = requests.get("https://developers.lingvolive.com/api/v1/Translation",
                                  {'text': word,
                                   'srcLang': languages.get(lang)[0],
                                   'dstLang': languages.get(lang)[1]},
                                  headers={'Authorization': f"Bearer {bearer_token}"}
                                  )

    if translate_resp.status_code != 200:
        raise Exception(f'Failed to do translation with error code {translate_resp.status_code}')

    translate_resp = translate_resp.json()

    dict_name = translate_resp[0]['Dictionary']

    for i in range(10):
        try:
            file_name = translate_resp[0]['Body'][0]['Markup'][i]['FileName']
            break
        except KeyError:
            continue

    if 'file_name' not in locals():
        try:
            file_name = translate_resp[0]['Body'][0]['Items'][0]['Markup'][0]['Markup'][0]['FileName']
        except KeyError:
            raise Exception(
                f'Unable to find FileName in JSON respose from translate call, try to findout why it is:\n{translate_resp}')

    sound_resp = requests.get("https://developers.lingvolive.com/api/v1/Sound",
                              {'dictionaryName': dict_name,
                               'fileName': file_name},
                              headers={'Authorization': f"Bearer {bearer_token}"})

    binary_wav = base64.b64decode(sound_resp.text[1:-1])

    file_dir = './abbyy_sounds/'

    if custom_file_name is not None:
        file_name = custom_file_name

    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    file = open(file_dir + file_name, mode='wb')
    file.write(binary_wav)
    file.close()

    return file_name

