import requests
import json
from config import Config

ya_token = Config.yandex_api_token

url = "https://translate.yandex.net/api/v1.5/tr.json/"


def language_definition(text):
    get_lang = requests.get(f"{url}detect?key={ya_token}&hint=en,ru&text={str(text)}")
    lang = json.loads(get_lang.text)['lang']
    res = "ru" if not lang else lang
    return res


def translate_word(text):
    lang = language_definition(text)
    if lang != "en":
        dest_lang = "en"
    else:
        dest_lang = "ru"
    get_translate = requests.get(f"{url}translate?key={ya_token}&lang={lang}-{dest_lang}&text={text}")
    res = json.loads(get_translate.text)
    return res['lang'] + '\n' + res['text'][0]
