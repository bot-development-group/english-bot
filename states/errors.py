class UserNotFoundException(Exception):
    pass


class AnotherTestException(Exception):
    pass
